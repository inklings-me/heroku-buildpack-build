build_failed() {
  local warn=$(cat $warnings)
  head "Build failed"
  echo ""
  info "We're sorry this build is failing!"
  info ""
  info "Are you running into a common issue?"
  info "https://devcenter.heroku.com/articles/troubleshooting-node-deploys"
  info ""
  if [ "$warn" != "" ]; then
    info "During the build we spotted some likely problems:"
    info ""
    echo "$warn" | indent
  else
    info "If you're stuck, please submit a ticket so we can help:"
    info "https://help.heroku.com/"
  fi
  info ""
  info "Love,"
  info "Heroku"
}

build_succeeded() {
  head "Build succeeded!"
  echo ""
  # (ls -R $build_dir/build || true) 2>/dev/null | indent
}

get_build_method() {
  local build_dir=$1
  if [[ $(read_json "$build_dir/package.json" ".scripts.build") != "" ]]; then
    echo "npm run build --" # requires npm 2.x
  elif test -f $build_dir/gulpfile.js && [ `gulp --tasks-simple | grep ^build$` == "build" ]; then
    echo "gulp build"
  else
    echo ""
  fi
}

get_modules_source() {
  local build_dir=$1
  if test -d $build_dir/node_modules; then
    echo "prebuilt"
  elif test -f $build_dir/npm-shrinkwrap.json; then
    echo "npm-shrinkwrap.json"
  elif test -f $build_dir/package.json; then
    echo "package.json"
  else
    echo ""
  fi
}

# Sets:
# iojs_engine
# node_engine
# npm_engine
# modules_source
# environment variables (from ENV_DIR)

read_current_state() {
  info "package.json..."
  assert_json "$build_dir/package.json"
  iojs_engine=$(read_json "$build_dir/package.json" ".engines.iojs")
  node_engine=$(read_json "$build_dir/package.json" ".engines.node")
  npm_engine=$(read_json "$build_dir/package.json" ".engines.npm")

  info "build directory..."
  modules_source=$(get_modules_source "$build_dir")

  info "environment variables..."
  export_env_dir $env_dir
  export NPM_CONFIG_PRODUCTION=${NPM_CONFIG_PRODUCTION:-false}
  export NODE_MODULES_CACHE=${NODE_MODULES_CACHE:-true}
}

show_current_state() {
  echo ""
  if [ "$iojs_engine" == "" ]; then
    info "Node engine:         ${node_engine:-unspecified}"
  else
    achievement "iojs"
    info "Node engine:         $iojs_engine (iojs)"
  fi
  info "Npm engine:          ${npm_engine:-unspecified}"
  info "node_modules source: ${modules_source:-none}"
  echo ""

  printenv | grep ^NPM_CONFIG_ | indent
  info "NODE_MODULES_CACHE=$NODE_MODULES_CACHE"
}

function build_dependencies() {
  if [ "$modules_source" == "" ]; then
    info "Skipping dependencies (no source for node_modules)"
  else
    info "Installing node devDependencies"
    env NPM_CONFIG_PRODUCTION="false" npm install --unsafe-perm --quiet --userconfig $build_dir/.npmrc 2>&1 | indent
  fi
}

clean_build_dir() {
    local target_dir=$1
    shopt -s nullglob
    for filepath in $target_dir/*; do
        if [ ! -d "$filepath" ]; then
            if [[ "$filepath" != "$build_dir/build/dependencies.json" ]]; then
                if [[ "$filepath" != "$build_dir/build/revved.json" ]]; then
                    array_contains keep_files "${filepath/$build_dir\/build\//}" || rm "$filepath"            
                fi
            fi
        else
            clean_build_dir "$filepath"
        fi
    done
    local remaining_files=($target_dir/*)
    if [[ ${#remaining_files[@]} == 0 ]]; then
       rm -r "$target_dir"
    fi
}

execute_build_method() {
  head "Executing build method"

  # Copy cache_dir/build to build_dir/build and clean build_dir/build
  if test -d $cache_dir/build; then
    cp -r $cache_dir/build $build_dir
    keep_files=($(read_values "$build_dir/build/revved.json"))
    clean_build_dir "$build_dir/build"
  fi

  # Clean cache_dir/build and build new files into cache_dir/build
  $build_method --build-dir="$cache_dir/build" --cache-dir="$build_dir/build" | indent

  # Merge revved.json files and copy cache_dir/build to build_dir/build
  if test -d $cache_dir/build; then
    cp $build_dir/revved.json $cache_dir/build/revved.json
    if test -d $build_dir/build; then
      info "Merging current manifest with cached manifest"
      local revvedJson=$(cat "$build_dir/build/revved.json")
      echo ${revvedJson//  \"/  \"obsolete/} | $bp_dir/vendor/jq --raw-output --slurp add "$cache_dir/build/revved.json" "/dev/stdin" > "$build_dir/revved.json"
    fi
    cp -r $cache_dir/build $build_dir
  fi
}

execute_build() {
  local build_method=$1
  local build_dir=$2
  if [ "$build_method" == "npm run build --" ]; then
    info "Found npm build script"
    execute_build_method
  elif [ "$build_method" == "gulp build" ]; then
    info "Found gulp build task"
    execute_build_method
  else
    info "None found"
  fi
}

clean_npm() {
  info "Cleaning npm artifacts"
  rm -rf "$build_dir/.node-gyp"
  rm -rf "$build_dir/.npm"
}

prune_devDependencies() {
  if [ ${NODE_ENV:-unspecified} == "production" ]; then
    info "Pruning devDependencies"
    npm --unsafe-perm prune --production 2>&1 | indent
  fi
}

# Caching
create_signature() {
  echo "$(node --version); $(npm --version)"
}

save_signature() {
  echo "$(create_signature)" > $cache_dir/node/signature
}

create_cache() {
  info "Caching results for future builds"
  mkdir -p $cache_dir/node

  echo `node --version` > $cache_dir/node/node-version
  echo `npm --version` > $cache_dir/node/npm-version
  save_signature

  if test -d $build_dir/node_modules; then
    cp -r $build_dir/node_modules $cache_dir/node
  fi
  write_user_cache
}

clean_cache() {
  info "Cleaning previous cache"
  rm -rf "$cache_dir/node_modules" # (for apps still on the older caching strategy)
  rm -rf "$cache_dir/node"
}

cache_directories() {
  local package_json="$build_dir/package.json"
  local key=".cache_directories"
  local check=$(key_exist $package_json $key)
  local result=-1
  if [ "$check" != -1 ]; then
    result=$(read_json "$package_json" "$key[]")
  fi
  local key=".cacheDirectories"
  local check=$(key_exist $package_json $key)
  if [ "$check" != -1 ]; then
    result=$(read_json "$package_json" "$key[]")
  fi
  echo $result
}

key_exist() {
  local file=$1
  local key=$2
  local output=$(read_json $file $key)
  if [ -n "$output" ]; then
    echo 1
  else
    echo -1
  fi
}

write_user_cache() {
  local directories=($(cache_directories))
  if [ "$directories" != -1 ]; then
    info "Storing directories:"
    for directory in "${directories[@]}"
    do
      info "- $directory"
      cp -r $build_dir/$directory $cache_dir/node/
    done
  fi
}
